package prisma_dumper

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"text/template"
	"time"
)

type PrismaDump struct {
	DumpVersion   string
	ServerVersion string
	ProjectId   string
	Tables        []*PrismaTable
	CompleteTime  string
}

type PrismaTable struct {
	Name   string
	Values string
}

func getSchemaTemplate() string {
	rawtemplate := `-- Go Prisma project Dump {{ .DumpVersion }}
--
-- ------------------------------------------------------
-- Server version	{{ .ServerVersion }}

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

USE prisma;

DELETE FROM Migration WHERE projectId="""'""";
DELETE FROM Project WHERE id="""'""";

{{range .Tables}}
-- Insert values into {{.Name}}
INSERT INTO {{ .Name }} VALUES {{ .Values }};
{{end}}

-- Dump completed on {{ .CompleteTime }}
`
	return strings.Replace(rawtemplate, "{{ .Name }}", "`{{ .Name }}`", -1)
}


func SchemaDump(db *sql.DB, projectId string) (string, error) {

	q, err := db.Query("USE `prisma`;")
	if err != nil {
		return "", err
	}
	q.Close()

	data := PrismaDump{
		DumpVersion:  version,
		Tables:       make([]*PrismaTable, 0),
		ProjectId:    projectId,
	}

	projects, err := getProjects(db)
	if err != nil {
		return "", err
	}
	if !projectExists(projectId, projects) {
		return "", errors.New("project not found")
	}

	// Get server version
	if data.ServerVersion, err = getServerVersion(db); err != nil {
		return "", err
	}

	// Get tables
	values, err := getMigrationValues(db, projectId)
	data.Tables = append(data.Tables, &PrismaTable{"Migration", values})
	values, err = getProjectValues(db, projectId)
	data.Tables = append(data.Tables, &PrismaTable{"Project", values})

	// Set complete time
	data.CompleteTime = time.Now().String()

	// Write dump to file
	t, err := template.New("mysqldump").Parse(getSchemaTemplate())
	if err != nil {
		return "", err
	}
	var buf bytes.Buffer
	if err = t.Execute(&buf, data); err != nil {
		return "", err
	}

	return buf.String(), nil
}

func projectExists(a string, projects []string) bool {
	for _, b := range projects {
		if b == a {
			return true
		}
	}
	return false
}


func getProjects(db *sql.DB) ([]string, error) {
	projects := make([]string, 0)

	rows, err := db.Query(fmt.Sprintf("SELECT `id` FROM `Project`"))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		id := ""
		rows.Scan(&id)
		projects = append(projects, id)
	}
	return projects, nil
}

func getProjectValues(db *sql.DB, projectId string) (string, error) {
	// Get Data
	rows, err := db.Query(fmt.Sprintf("SELECT * FROM `Project` WHERE id='%s'", projectId))
	if err != nil {
		return "", err
	}
	defer rows.Close()

	// Get columns
	columns, err := rows.Columns()
	if err != nil {
		return "", err
	}
	if len(columns) == 0 {
		return "", errors.New("no columns in table 'Project'")
	}

	// Read data
	dataText := make([]string, 0)
	for rows.Next() {
		// Init temp data storage

		data := make([]*sql.NullString, len(columns))
		ptrs := make([]interface{}, len(columns))
		for i, _ := range data {
			ptrs[i] = &data[i]
		}

		// Read data
		if err := rows.Scan(ptrs...); err != nil {
			return "", err
		}

		dataStrings := make([]string, len(columns))

		for key, value := range data {
			if value != nil && value.Valid {
				if value.String == projectId {
					value.String = `""'""`
				}
				dataStrings[key] = "'" + value.String + "'"
			} else {
				dataStrings[key] = "null"
			}
		}

		dataText = append(dataText, "("+strings.Join(dataStrings, ",")+")")
	}

	return strings.Join(dataText, ","), rows.Err()
}

func getMigrationValues(db *sql.DB, projectId string) (string, error) {
	// Get Data
	rows, err := db.Query(fmt.Sprintf("SELECT * FROM `Migration` WHERE projectId='%s'", projectId))
	if err != nil {
		return "", err
	}
	defer rows.Close()

	// Get columns
	columns, err := rows.Columns()
	if err != nil {
		return "", err
	}
	if len(columns) == 0 {
		return "", errors.New("no columns in table 'Migration'")
	}

	// Read data
	dataText := make([]string, 0)
	for rows.Next() {
		// Init temp data storage

		data := make([]*sql.NullString, len(columns))
		ptrs := make([]interface{}, len(columns))
		for i, _ := range data {
			ptrs[i] = &data[i]
		}

		// Read data
		if err := rows.Scan(ptrs...); err != nil {
			return "", err
		}

		dataStrings := make([]string, len(columns))

		for key, value := range data {
			if value != nil && value.Valid {
				if value.String == projectId {
					value.String = `""'""`
				}
				dataStrings[key] = "'" + value.String + "'"
			} else {
				dataStrings[key] = "null"
			}
		}

		dataText = append(dataText, "("+strings.Join(dataStrings, ",")+")")
	}

	return strings.Join(dataText, ","), rows.Err()
}